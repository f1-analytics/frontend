import { ActionType, AllActions } from './drivers.actions.types';
import { DriversState } from './drivers.reducer.types';
import { actionFulfilled, actionPending } from '../actions.types';

export const initialState: DriversState = {
    drivers: [],
    isPending: false,
    selectedDriver: null,
};

export default (state = initialState, action: AllActions): DriversState => {
    switch (action.type) {
        case actionPending(ActionType.driversFetched):
            return {
                ...state,
                isPending: true,
            };
        case actionFulfilled(ActionType.driversFetched):
            return {
                ...state,
                drivers: action.payload,
                isPending: false,
                selectedDriver: state.selectedDriver || action.payload[0],
            };
        case ActionType.driverSelected:
            return {
                ...state,
                selectedDriver: action.payload,
            };
        default:
            return state;
    }
};
