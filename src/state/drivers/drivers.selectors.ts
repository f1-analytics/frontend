import { RootState } from '../reducers.types';

export const driversSelector = (state: RootState) => state.drivers.drivers;
export const selectedDriverSelector = (state: RootState) => state.drivers.selectedDriver;
