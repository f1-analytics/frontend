import { Driver } from 'api';

export interface DriversState {
    drivers: Driver[];
    isPending: boolean;
    selectedDriver: Driver | null;
}
