import { Driver } from 'api';
import { FulfilledActionType, PendingActionType } from '../actions.types';

export enum ActionType {
    driversFetched = 'drivers/DRIVERS_FETCHED',
    driverSelected = 'drivers/DRIVER_SELECTED',
}

export interface FetchDrivers {
    type: PendingActionType<ActionType.driversFetched>;
}

export interface FetchDriversResolved {
    type: FulfilledActionType<ActionType.driversFetched>;
    payload: Driver[];
}

export interface SelectDriver {
    type: ActionType.driverSelected;
    payload: Driver;
}

export type AllActions = FetchDrivers | FetchDriversResolved | SelectDriver;
