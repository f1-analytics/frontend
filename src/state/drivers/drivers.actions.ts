import api, { Driver } from 'api';
import { resolveRequest } from '../api.actions';
import { ActionType, SelectDriver } from './drivers.actions.types';
import { ThunkDispatch } from '../actions.types';

export const fetchDrivers = () => (dispatch: ThunkDispatch): void => {
    dispatch(resolveRequest(api.data.dataDriversList(), ActionType.driversFetched));
};

export const setSelectedDriver = (driver: Driver): SelectDriver => ({
    type: ActionType.driverSelected,
    payload: driver,
});
