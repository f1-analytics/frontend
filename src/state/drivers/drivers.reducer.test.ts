import DriverBuilder from 'test-helpers/data-builders/driver.builder';
import reducer, { initialState } from './drivers.reducer';
import { ActionType, AllActions } from './drivers.actions.types';
import { setSelectedDriver } from './drivers.actions';
import { actionFulfilled } from '../actions.types';
import { driversSelector, selectedDriverSelector } from './drivers.selectors';
import { DriversState } from './drivers.reducer.types';
import { RootState } from '../reducers.types';

function getRootState(driversState: DriversState): RootState {
    return { drivers: driversState };
}

describe('Drivers reducer slice', () => {
    it('should return initial state', () => {
        const actual = reducer(undefined, {} as AllActions);

        expect(actual).toEqual(initialState);
    });

    it('should select driver', () => {
        const driver = new DriverBuilder().build();

        const nextState = reducer(undefined, setSelectedDriver(driver));

        const rootState = getRootState(nextState);
        expect(selectedDriverSelector(rootState)).toEqual(driver);
    });

    it('should save fetched drivers and pre-select driver', () => {
        const drivers = [new DriverBuilder().build(), new DriverBuilder().build()];

        const nextState = reducer(undefined, { type: actionFulfilled(ActionType.driversFetched), payload: drivers });

        const rootState = getRootState(nextState);
        expect(driversSelector(rootState)).toEqual(drivers);
        expect(selectedDriverSelector(rootState)).toEqual(drivers[0]);
    });

    it('should save fetched drivers and not override already selected driver', () => {
        const selectedDriver = new DriverBuilder().build();
        const drivers = [new DriverBuilder().build(), new DriverBuilder().build()];

        let nextState = reducer(undefined, setSelectedDriver(selectedDriver));
        nextState = reducer(nextState, { type: actionFulfilled(ActionType.driversFetched), payload: drivers });

        const rootState = getRootState(nextState);
        expect(driversSelector(rootState)).toEqual(drivers);
        expect(selectedDriverSelector(rootState)).toEqual(selectedDriver);
    });
});
