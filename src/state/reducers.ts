import { combineReducers } from 'redux';
import drivers from './drivers/drivers.reducer';

export default combineReducers({
    drivers,
});
