import { ThunkDispatch as ImportedThunkDispatch } from 'redux-thunk';
import { RootState } from './reducers.types';
import { AnyAction } from 'redux';

export enum ActionType {
    Pending = 'PENDING',
    Fulfilled = 'FULFILLED',
    Rejected = 'REJECTED',
}

export type ThunkDispatch = ImportedThunkDispatch<RootState, undefined, AnyAction>;

/* eslint-disable */

export type PendingActionType<ActionName extends string> = `${ActionName}_${ActionType.Pending}`;
export type FulfilledActionType<ActionName extends string> = `${ActionName}_${ActionType.Fulfilled}`;
export type RejectedActionType<ActionName extends string> = `${ActionName}_${ActionType.Rejected}`;

export const actionPending = <ActionName extends string>(actionName: ActionName) =>
    `${actionName}_${ActionType.Pending}` as PendingActionType<ActionName>;

export const actionFulfilled = <ActionName extends string>(actionName: ActionName) =>
    `${actionName}_${ActionType.Fulfilled}` as FulfilledActionType<ActionName>;

export const actionRejected = <ActionName extends string>(actionName: ActionName) =>
    `${actionName}_${ActionType.Rejected}` as RejectedActionType<ActionName>;
