import { DriversState } from './drivers/drivers.reducer.types';

export interface RootState {
    drivers: DriversState;
}
