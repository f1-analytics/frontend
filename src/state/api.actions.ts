import { Dispatch } from 'redux';
import { HttpResponse } from 'api';
import { actionFulfilled, actionPending, actionRejected } from './actions.types';

export const resolveRequest = (requestPromise: Promise<HttpResponse<unknown>>, actionType: string) => (
    dispatch: Dispatch,
): void => {
    dispatch({ type: actionPending(actionType) });
    requestPromise
        .then((result) => dispatch({ type: actionFulfilled(actionType), payload: result.data }))
        .catch((error) => dispatch({ type: actionRejected(actionType), payload: error }));
};
