import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import useStyles from './Header.styles';
import DriverSelection from './DriverSelection/DriverSelection';

const Header: React.FC = () => {
    const classes = useStyles();
    return (
        <Toolbar>
            <div className={classes.contentContainer}>
                <div className={classes.content}>
                    <DriverSelection />
                </div>
            </div>
        </Toolbar>
    );
};

export default Header;
