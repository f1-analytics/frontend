import makeStyles from '@material-ui/core/styles/makeStyles';
import { lighten, Theme } from '@material-ui/core';

const getSuggestionHeight = (theme: Theme) => `${theme.spacing(8)}px`;

export default makeStyles((theme) => ({
    root: {
        position: 'absolute',
        left: 0,
        right: 0,
        marginTop: theme.spacing(4),
        overflow: 'hidden',
    },
    suggestion: {
        height: getSuggestionHeight(theme),
        lineHeight: getSuggestionHeight(theme),
        paddingLeft: theme.spacing(4),
        transition: theme.transitions.create('background-color', {
            duration: theme.transitions.duration.shortest,
        }),
        '&:hover': {
            background: lighten(theme.palette.primary.light, 0),
        },
    },
}));
