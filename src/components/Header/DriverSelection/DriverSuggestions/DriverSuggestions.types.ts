import { Driver } from 'api';

export interface DriverSuggestionsProps {
    isShown: boolean;
    drivers: Driver[];
    currentInput: string;
    onClose: () => void;
    onSelect: (driver: Driver) => void;
}
