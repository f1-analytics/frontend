import React from 'react';
import { mount } from 'enzyme';
import Fade from '@material-ui/core/Fade';
import DriverBuilder from 'test-helpers/data-builders/driver.builder';
import DriverSuggestions, { MAX_SUGGESTIONS } from './DriverSuggestions';
import { DriverSuggestionsProps } from './DriverSuggestions.types';
import { getDriverFullName } from '../DriverSelection.helpers';

describe('DriverSuggestions component tests', () => {
    const props: DriverSuggestionsProps = {
        isShown: true,
        drivers: [new DriverBuilder().build(), new DriverBuilder().build()],
        currentInput: '',
        onClose: jest.fn(),
        onSelect: jest.fn(),
    };

    it('should not be visible if not shown', () => {
        const wrapper = mount(<DriverSuggestions {...props} isShown={false} />);

        expect(wrapper.find(Fade).props().in).toBe(false);
    });

    it('should be visible if shown', () => {
        const wrapper = mount(<DriverSuggestions {...props} isShown={true} />);

        expect(wrapper.find(Fade).props().in).toBe(true);
    });

    it('should show driver suggestions until max limit', () => {
        const drivers = [...Array(MAX_SUGGESTIONS + 3)].map((_, index) =>
            new DriverBuilder().withFullName(`forename_${index}`, `surename_${index}`).build(),
        );

        const wrapper = mount(<DriverSuggestions {...props} currentInput='' drivers={drivers} />);

        for (let index = 0; index < MAX_SUGGESTIONS; index++) {
            expect(wrapper.text()).toContain(`forename_${index} surename_${index}`);
        }
    });

    it('should not show driver suggestions after max limit', () => {
        const drivers = [...Array(MAX_SUGGESTIONS + 3)].map((_, index) =>
            new DriverBuilder().withFullName(`forename_${index}`, `surename_${index}`).build(),
        );

        const wrapper = mount(<DriverSuggestions {...props} currentInput='' drivers={drivers} />);

        for (let index = MAX_SUGGESTIONS; index < MAX_SUGGESTIONS + 3; index++) {
            expect(wrapper.text()).not.toContain(`forename_${index} surename_${index}`);
        }
    });

    it('should correctly filter suggestions by input', () => {
        const drivers = [
            new DriverBuilder().withFullName('Fernando', 'Alonso').build(),
            new DriverBuilder().withFullName('Fernando', 'Lorenzo').build(),
            new DriverBuilder().withFullName('Mark', 'Webber').build(),
        ];

        const wrapper = mount(<DriverSuggestions {...props} currentInput='Fernando' drivers={drivers} />);

        expect(wrapper.text()).toContain(getDriverFullName(drivers[0]));
        expect(wrapper.text()).toContain(getDriverFullName(drivers[1]));
        expect(wrapper.text()).not.toContain(getDriverFullName(drivers[2]));
    });

    it('should close suggestions on click outside component', () => {
        const mockedOnClose = jest.fn();

        mount(<DriverSuggestions {...props} onClose={mockedOnClose} />);
        window.document.body.dispatchEvent(new Event('click'));

        expect(mockedOnClose).toHaveBeenCalled();
    });

    it('should close suggestions on escape click', () => {
        const mockedOnClose = jest.fn();

        mount(<DriverSuggestions {...props} onClose={mockedOnClose} />);
        window.document.body.dispatchEvent(new KeyboardEvent('keydown', { key: 'Escape' }));

        expect(mockedOnClose).toHaveBeenCalled();
    });
});
