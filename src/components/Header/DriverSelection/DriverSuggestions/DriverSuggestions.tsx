import React, { useEffect, useRef, useState, useCallback } from 'react';
import debounce from 'lodash.debounce';
import Paper from '@material-ui/core/Paper/Paper';
import Typography from '@material-ui/core/Typography';
import Fade from '@material-ui/core/Fade';
import { Driver } from 'api';
import useStyles from './DriverSuggestions.styles';
import { DriverSuggestionsProps } from './DriverSuggestions.types';
import { getDriverFullName } from '../DriverSelection.helpers';

const DEBOUNCE_TIMER = 300;
const TRANSITION_TIMER = 300;
export const MAX_SUGGESTIONS = 5;

const DriverSuggestions: React.FC<DriverSuggestionsProps> = ({ isShown, drivers, currentInput, onClose, onSelect }) => {
    const classes = useStyles();
    const [driverSuggestions, setDriverSuggestions] = useState<Driver[]>([]);
    const ref = useRef<HTMLDivElement | null>(null);

    const handleHideDropdown = useCallback(
        (event: KeyboardEvent) => {
            if (event.key === 'Escape') {
                onClose();
            }
        },
        [onClose],
    );

    const handleClickOutside = useCallback(
        (event: Event) => {
            if (!ref.current?.contains(event.target as Node)) {
                onClose();
            }
        },
        [onClose],
    );

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const debouncedSetFilteredSuggestions = useCallback(
        debounce((driverFilter) => {
            setDriverSuggestions(
                drivers.filter((driver) =>
                    getDriverFullName(driver).toLowerCase().includes(driverFilter.toLowerCase()),
                ),
            );
        }, DEBOUNCE_TIMER),
        [drivers],
    );

    useEffect(() => debouncedSetFilteredSuggestions(currentInput), [debouncedSetFilteredSuggestions, currentInput]);

    useEffect(() => {
        document.addEventListener('keydown', handleHideDropdown, true);
        document.addEventListener('click', handleClickOutside, true);
        return () => {
            document.removeEventListener('keydown', handleHideDropdown, true);
            document.removeEventListener('click', handleClickOutside, true);
        };
    }, [handleHideDropdown, handleClickOutside]);

    return (
        <Fade in={isShown} timeout={TRANSITION_TIMER}>
            <Paper ref={ref} className={classes.root}>
                {driverSuggestions.slice(0, MAX_SUGGESTIONS).map((driver) => (
                    <Typography key={driver.id} className={classes.suggestion} onClick={() => onSelect(driver)}>
                        {getDriverFullName(driver)}
                    </Typography>
                ))}
            </Paper>
        </Fade>
    );
};

export default DriverSuggestions;
