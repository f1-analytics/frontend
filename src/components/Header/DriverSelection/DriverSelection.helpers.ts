import { Driver } from 'api';

export const getDriverFullName = (driver: Driver): string => `${driver.forename} ${driver.surname}`;
