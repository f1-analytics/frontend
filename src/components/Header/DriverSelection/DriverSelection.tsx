import React, { useEffect, useState, useRef, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { driversSelector, selectedDriverSelector } from 'state/drivers/drivers.selectors';
import { fetchDrivers, setSelectedDriver } from 'state/drivers/drivers.actions';
import { Driver } from 'api';
import useStyles from './DriverSelection.styles';
import DriverSuggestions from './DriverSuggestions/DriverSuggestions';
import { getDriverFullName } from './DriverSelection.helpers';
import DriverInput from './DriverInput/DriverInput';

const DriverSelection: React.FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [inputValue, setInputValue] = useState('');
    const [shouldShowSuggestions, setShouldShowSuggestions] = useState(false);
    const inputRef = useRef<HTMLInputElement | null>(null);

    const drivers = useSelector(driversSelector);
    const selectedDriver = useSelector(selectedDriverSelector);

    function focusOnInput() {
        inputRef?.current?.focus();
    }

    function handleInputChange(value: string) {
        setInputValue(value);
        setShouldShowSuggestions(true);
    }

    function handleDriverSelect(driver: Driver) {
        dispatch(setSelectedDriver(driver));
        setInputValue(getDriverFullName(driver));
        setShouldShowSuggestions(false);
    }

    const handleSuggestionsClose = useCallback(() => {
        setShouldShowSuggestions(false);
        if (selectedDriver) {
            setInputValue(getDriverFullName(selectedDriver));
        }
    }, [selectedDriver]);

    useEffect(() => {
        dispatch(fetchDrivers());
    }, [dispatch]);

    useEffect(() => {
        if (selectedDriver) {
            setInputValue(getDriverFullName(selectedDriver));
        }
    }, [selectedDriver]);

    return (
        <div className={classes.root}>
            {selectedDriver && (
                <DriverInput
                    inputRef={inputRef}
                    value={inputValue}
                    onChange={handleInputChange}
                    onSearchClick={focusOnInput}
                />
            )}
            <DriverSuggestions
                isShown={Boolean(shouldShowSuggestions && drivers)}
                drivers={drivers}
                currentInput={inputValue}
                onClose={handleSuggestionsClose}
                onSelect={handleDriverSelect}
            />
        </div>
    );
};

export default DriverSelection;
