import React from 'react';

export interface DriverInputProps {
    value: string;
    onChange: (inputValue: string) => void;
    onSearchClick: () => void;
    inputRef: React.Ref<HTMLInputElement | null>;
}
