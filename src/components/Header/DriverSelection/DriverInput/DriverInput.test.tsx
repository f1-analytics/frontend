import React from 'react';
import renderer from 'react-test-renderer';
import DriverInput from './DriverInput';

describe('DriverInput component tests', () => {
    it('should match snapshot', () => {
        const wrapper = renderer.create(
            <DriverInput value='Fernando Alonso' onSearchClick={jest.fn()} onChange={jest.fn()} inputRef={null} />,
        );

        expect(wrapper).toMatchSnapshot();
    });
});
