import React from 'react';
import TextField from '@material-ui/core/TextField/TextField';
import SearchIcon from '@material-ui/icons/Search';
import useStyles from './DriverInput.styles';
import { DriverInputProps } from './DriverInput.types';

const DriverInput: React.FC<DriverInputProps> = ({ value, onChange, onSearchClick, inputRef }) => {
    const classes = useStyles();

    return (
        <>
            <TextField
                className={classes.textField}
                InputProps={{ className: classes.input }}
                inputRef={inputRef}
                value={value}
                onChange={(event) => onChange(event.target.value)}
            />
            <SearchIcon className={classes.searchIcon} onClick={onSearchClick} />
        </>
    );
};

export default DriverInput;
