import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles((theme) => ({
    searchIcon: {
        height: '100% !important',
        position: 'absolute',
        right: '0',
        color: theme.palette.text.primary,
    },
    textField: {
        width: '100%',
    },
    input: {
        color: theme.palette.text.primary,
        width: '100%',
        marginRight: theme.spacing(6),
        fontSize: theme.spacing(5.5),
        '&:before': {
            borderBottomColor: theme.palette.text.primary, // Semi-transparent underline
            opacity: 0,
        },
        '&:hover:before': {
            borderBottomColor: theme.palette.text.primary, // Solid underline on hover
        },
        '&:after': {
            borderBottomColor: theme.palette.text.primary, // Solid underline on focus
        },
    },
}));
