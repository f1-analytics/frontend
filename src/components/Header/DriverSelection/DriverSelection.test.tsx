import React from 'react';
import { mount } from 'enzyme';
import { mockUseDispatch, mockUseSelector } from 'test-helpers/test.utils';
import DriverBuilder from 'test-helpers/data-builders/driver.builder';
import * as DriversSelectors from 'state/drivers/drivers.selectors';
import { actionPending } from 'state/actions.types';
import { ActionType } from 'state/drivers/drivers.actions.types';
import { setSelectedDriver } from 'state/drivers/drivers.actions';
import DriverSelection from './DriverSelection';
import DriverInput from './DriverInput/DriverInput';
import { getDriverFullName } from './DriverSelection.helpers';
import DriverSuggestions from './DriverSuggestions/DriverSuggestions';

describe('DriverSelection component tests', () => {
    let dispatchedActions: unknown[] = [];

    beforeEach(() => {
        dispatchedActions = mockUseDispatch();
        mockUseSelector();
        jest.spyOn(DriversSelectors, 'selectedDriverSelector').mockReturnValue(new DriverBuilder().build());
        jest.spyOn(DriversSelectors, 'driversSelector').mockReturnValue([
            new DriverBuilder().build(),
            new DriverBuilder().build(),
        ]);
    });

    it('should fetch drivers on mount', () => {
        mount(<DriverSelection />);

        expect(dispatchedActions).toContainEqual({ type: actionPending(ActionType.driversFetched) });
    });

    it('should set input with selected driver full name', () => {
        const driver = new DriverBuilder().build();
        jest.spyOn(DriversSelectors, 'selectedDriverSelector').mockReturnValue(driver);

        const wrapper = mount(<DriverSelection />);

        expect(wrapper.find(DriverInput).props().value).toBe(getDriverFullName(driver));
        expect(wrapper.find(DriverSuggestions).props().currentInput).toBe(getDriverFullName(driver));
    });

    it('should save driver input change', () => {
        const mockedInput = 'Fernando';
        const wrapper = mount(<DriverSelection />);

        wrapper.find(DriverInput).props().onChange(mockedInput);
        wrapper.update();

        expect(wrapper.find(DriverInput).props().value).toBe(mockedInput);
    });

    it('should open suggestions on driver input change', () => {
        const wrapper = mount(<DriverSelection />);

        expect(wrapper.find(DriverSuggestions).props().isShown).toBe(false);

        wrapper.find(DriverInput).props().onChange('Fernando');
        wrapper.update();

        expect(wrapper.find(DriverSuggestions).props().isShown).toBe(true);
    });

    it('should save selected driver', () => {
        const driver = new DriverBuilder().build();
        const wrapper = mount(<DriverSelection />);

        wrapper.find(DriverSuggestions).props().onSelect(driver);

        expect(dispatchedActions).toContainEqual(setSelectedDriver(driver));
    });

    it('should save driver full name as input on driver select', () => {
        const driver = new DriverBuilder().build();
        const wrapper = mount(<DriverSelection />);

        wrapper.find(DriverSuggestions).props().onSelect(driver);
        wrapper.update();

        expect(wrapper.find(DriverInput).props().value).toBe(getDriverFullName(driver));
    });

    it('should close driver suggestions on driver select', () => {
        const driver = new DriverBuilder().build();
        const wrapper = mount(<DriverSelection />);

        // Open driver suggestions
        wrapper.find(DriverInput).props().onChange('Fernando');
        wrapper.update();
        expect(wrapper.find(DriverSuggestions).props().isShown).toBe(true);

        wrapper.find(DriverSuggestions).props().onSelect(driver);
        wrapper.update();

        expect(wrapper.find(DriverSuggestions).props().isShown).toBe(false);
    });

    it('should handle manually closing driver suggestions', () => {
        const wrapper = mount(<DriverSelection />);

        // Open driver suggestions
        wrapper.find(DriverInput).props().onChange('Fernando');
        wrapper.update();
        expect(wrapper.find(DriverSuggestions).props().isShown).toBe(true);

        wrapper.find(DriverSuggestions).props().onClose();
        wrapper.update();

        expect(wrapper.find(DriverSuggestions).props().isShown).toBe(false);
    });

    it('should reset input to selected driver after manually closing suggestions', () => {
        const driver = new DriverBuilder().build();
        jest.spyOn(DriversSelectors, 'selectedDriverSelector').mockReturnValue(driver);
        const wrapper = mount(<DriverSelection />);

        wrapper.find(DriverInput).props().onChange('Fernando');
        wrapper.update();
        expect(wrapper.find(DriverInput).props().value).toBe('Fernando');

        wrapper.find(DriverSuggestions).props().onClose();
        wrapper.update();

        expect(wrapper.find(DriverInput).props().value).toBe(getDriverFullName(driver));
    });
});
