import React from 'react';
import renderer from 'react-test-renderer';
import Header from './Header';

jest.mock('./DriverSelection/DriverSelection', () => () => <div />);

describe('Header component tests', () => {
    it('should match component snapshot', () => {
        const wrapper = renderer.create(<Header />);

        expect(wrapper).toMatchSnapshot();
    });
});
