import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles((theme) => ({
    contentContainer: {
        display: 'flex',
        justifyContent: 'center',
        width: '100%',
        padding: theme.spacing(0, 9),
        [theme.breakpoints.down('sm')]: {
            padding: theme.spacing(0, 4),
        },
    },
    content: {
        maxWidth: theme.spacing(400),
        width: '100%',
    },
}));
