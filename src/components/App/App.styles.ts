import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles((theme) => ({
    root: {
        height: '100%',
        backgroundColor: theme.palette.background.default,
    },
}));
