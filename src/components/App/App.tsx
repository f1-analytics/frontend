import React from 'react';
import Header from '../Header/Header';
import useStyles from './App.styles';

const App: React.FC = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Header />
        </div>
    );
};

export default App;
