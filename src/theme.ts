import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

export default createMuiTheme({
    spacing: 4,
    palette: {
        type: 'dark',
    },
});
