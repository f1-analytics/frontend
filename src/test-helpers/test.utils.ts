import * as ReactRedux from 'react-redux';

export const mockUseDispatch = (): unknown[] => {
    const dispatchedActions: unknown[] = [];

    function dispatchAction(action: unknown) {
        if (typeof action === 'function') {
            action(dispatchAction);
        }
        dispatchedActions.push(action);
    }

    // @ts-ignore
    jest.spyOn(ReactRedux, 'useDispatch').mockReturnValue(dispatchAction);

    return dispatchedActions;
};

export const mockUseSelector = () => {
    jest.spyOn(ReactRedux, 'useSelector').mockImplementation((fn) => fn({}));
};
