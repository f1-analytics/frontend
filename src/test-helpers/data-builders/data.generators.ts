export function generateRandomId(max = 100): string {
    return String(Math.floor(Math.random() * max - 1) + 1);
}

export function generateRandomString({ minLength = 0, maxLength = 40 } = {}): string {
    const length = Math.floor(Math.random() * maxLength - 1 - minLength) + minLength;
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ';
    let result = '';
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
}
