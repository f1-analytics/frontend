import { Driver } from 'api';
import { generateRandomId, generateRandomString } from './data.generators';

class DriverBuilder {
    private driver: Driver;

    constructor() {
        this.driver = {
            id: generateRandomId(),
            forename: generateRandomString(),
            surname: generateRandomString(),
        };
    }

    withFullName(forename: string, surname: string) {
        this.driver = { ...this.driver, forename, surname };
        return this;
    }

    build() {
        return this.driver;
    }
}

export default DriverBuilder;
