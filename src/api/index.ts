import { Api } from './generated-api';

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
    data: D | null;
    error: E | null;
}

const api = new Api({ baseUrl: process.env.REACT_APP_BACKEND_URL });

export default api;
export * from './generated-api';
