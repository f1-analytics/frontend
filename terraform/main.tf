terraform {
  backend "s3" {
    bucket = "tf-states-backend"
    key = "frontend.tfstate"
    region = "us-east-1"
  }
}

data "aws_iam_policy_document" "frontend_policy" {
  statement {
    actions = [
      "s3:GetObject"
    ]
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [
      "arn:aws:s3:::frontend-prod-build/*"
    ]
  }
}

resource "aws_s3_bucket" "frontend-build" {
  bucket = "frontend-prod-build"
  acl = "public-read"
  policy = data.aws_iam_policy_document.frontend_policy.json
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}
