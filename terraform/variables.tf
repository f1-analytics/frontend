variable "domain_name" {
  type = string
  default = "f1analytics.ml"
}

variable "build_bucket_name" {
  type = string
  default = "frontend-prod-build"
}
