module.exports = {
    webpack: (config) => webpackOverride(config),
};

function webpackOverride(config) {
    return {
        ...config,
        module: {
            ...config.module,
            rules: rulesWithoutUseEslintrc(config.module.rules),
        },
    };
}

function rulesWithoutUseEslintrc(rules) {
    return rules.filter((rule) => !ruleHasUseEslintrc(rule));
}

function ruleHasUseEslintrc(rule) {
    return rule.use && rule.use.some((use) => use.options && use.options.useEslintrc !== undefined);
}
